from .models import Technician, ServiceAppointment, AutomobileVO
from common.json import ModelEncoder
import json

# Create your views here.


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href"]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["name",
                  "employee_number",
                  ]

class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = ["automobile",
                  "customer_name",
                  "date",
                  "time",
                  "technician",
                  "reason",
                  "id",
                  "vip"]
    encoders = {
        "technician": TechnicianEncoder(),
    }

    def get_extra_data(self, o):
        date = json.dumps(o.date, default=str)
        time = json.dumps(o.time, default=str)
        date = json.loads(date)
        time = json.loads(time)
        return {"date": date,
                "time": time, }
