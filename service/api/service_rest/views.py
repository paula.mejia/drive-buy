from django.shortcuts import render
from .encoders import AutomobileVODetailEncoder, ServiceAppointmentEncoder, TechnicianEncoder
from .models import Technician, ServiceAppointment, AutomobileVO
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=ServiceAppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        appointmentvin = content["automobile"]
        automobiles = AutomobileVO.objects.all()
        automobilevins = []
        for automobile in automobiles:
            automobilevins.append(automobile.vin)
        if appointmentvin in automobilevins:
            content["vip"]=True

        automobile = content["automobile"]
        try:
            technician = Technician.objects.get(name=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "No technician with that name"},
                status=404,
            )
        service_appointment = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            service_appointment,
            encoder=ServiceAppointmentEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_show_appointment(request, id):
    if request.method == "GET":
        appointment = ServiceAppointment.objects.get(id=id)
        return JsonResponse(
            {"appointment": appointment},
            encoder=ServiceAppointmentEncoder,
        )
    else:
        try:
            appointment = ServiceAppointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse({"message": "deleted"})
        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_show_technician(request, id):
    if request.method == "GET":
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            technician = Technician.objects.get(id=id)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

@require_http_methods(["GET"])
def api_service_history(request, vin):
    if request.method == "GET":
        if vin == None:
            service_history = ServiceAppointment.objects.all()
        else:
            service_history = ServiceAppointment.objects.filter(automobile = vin)
        return JsonResponse(
            {"service_history": service_history},
            encoder=ServiceAppointmentEncoder,
        )
