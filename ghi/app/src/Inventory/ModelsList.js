import { useState, useEffect} from 'react'

function ModelsList() {
  const [models, setModels] = useState([])

  const getData = async () => {
    const resp = await fetch('http://localhost:8100/api/models/');

    if (resp.ok) {
      const data = await resp.json();
      setModels(data.models)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  return (
    <>
    <div>
      <h1 className="display-5 fw-bold">Vehicle models</h1>
    </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {models.map(model => {
          return (
            <tr>
              <td>{model.name}</td>
              <td>{model.manufacturer.name}</td>
              <td><img src={model.picture_url}/></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}

export default ModelsList;
