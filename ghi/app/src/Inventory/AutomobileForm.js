import React, {useState, useEffect} from 'react';

function AutomobileForm() {
//   const [color, setColor] = useState('')
//   const [year, setYear] = useState('')
//   const [vin, setVin] = useState('')
//   const [model_id, setModelId] = useState('')

//   const getData = async () => {
//     const resp = await fetch("http://localhost:8100/api/models/");

//     if (resp.ok) {
//       const data = await resp.json();
//       const models = data.autos.map((car) => {
//           return {
//             model_id: car.model.id
//        };
//     });

//       setModelId(models);
//      }
//   }}
//   const fetchData = async () => {
//     const url = 'http://localhost:8100/api/automobiles/';

//     const response = await fetch(url);

//     if (response.ok) {
//       const data = await response.json();
//     }
//   }

//   useEffect(() => {
//     fetchData();
// }, [])

//   useEffect(() => {
//     getData();
// }, [])
// const handleColorChange = (e) => {
//     setColor(e.target.value);
// }
// const handleYearChange = (e) => {
//       setYear(e.target.value);
// }
// const handleVinChange = (e) => {
//         setVin(e.target.value);
// }
// // const handleModelChange = (e) => {
// //           setModel(e.target.value);
// // }
// const handleSubmit = async (e) => {
//     e.preventDefault();

//     const data = {
//       "color": color,
//       "year": year,
//       "vin": vin,
//       // "model_id": model_id
//     }

//     const automobileURL = 'http://localhost:8100/api/automobiles/'
//     const fetchConfig = {
//       method: "post",
//       body: JSON.stringify(data),
//       headers: {
//         'Content-Type': 'application/json',
//       },
//     };

//     const response = await fetch(automobileURL, fetchConfig);

//     if (response.ok) {
//       const newAutomobile = await response.json();
//       console.log(newAutomobile)

//      setColor('');
//      setYear('');
//      setVin('');
//     //  setModel('');
//   }


//   return (
//     <div className="row">
//       <div className="offset-3 col-6">
//         <div className="shadow p-4 mt-4">
//           <h1>Create an Automobile</h1>
//           <form onSubmit={handleSubmit} id="add-automobile-form">
//             <div className="form-floating mb-3">
//               <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
//               <label htmlFor="color">Color</label>
//             </div>
//             <div className="form-floating mb-3">
//               <input onChange={handleYearChange} value={year} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
//               <label htmlFor="year">Year</label>
//             </div>
//             <div className="form-floating mb-3">
//               <input onChange={handleVinChange} value={vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
//               <label htmlFor="vin">VIN</label>
//             </div>
//             <button className="btn btn-primary">Add</button>
//           </form>
//         </div>
//       </div>
//     </div>
//   );
 }


export default AutomobileForm;

