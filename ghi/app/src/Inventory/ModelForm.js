import React, {useState, useEffect} from 'react';

function ModelForm() {
  const [name, setName] = useState('')
  const [picture_url, SetPictureURL] = useState('')
  const [manufacturer, SetManufacturer] = useState('')
  const [formData, setFormData] = useState({
    automobile:'',
    salesperson:'',
    customer:'',
    price:'',

})

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/models/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
    }
  }

  useEffect(() => {
    getData()
}, []);

async function getData() {
  const url = "http://localhost:8100/api/manufacturers/";

  const response = await fetch(url);

  if (response.ok) {
      const data = await response.json();
      SetManufacturer(data.manufacturer)
  }
}

const handleNameChange = (e) => {
    setName(e.target.value);
}
const handlePictureChange = (e) => {
  SetPictureURL(e.target.value)
}
const handleManufacturerChange = (e) => {
  SetManufacturer(e.target.value)
}

const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {
      "name": name,
      "picture_url": picture_url,
      "manufacturer": manufacturer,
    }

    const modelURL = 'http://localhost:8100/api/models/'
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(modelURL, fetchConfig);

    if (response.ok) {
      const newModel = await response.json();
      console.log(newModel)

     setName('');
  }
}

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Model</h1>
          <form onSubmit={handleSubmit} id="add-model-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictureChange} value={picture_url} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture</label>
            </div>
            <div className="mb-3">
                <select value={formData.manufacturer} onChange={handleManufacturerChange} required id="manufacturer" name="manufacturer" className="form-select">
                    <option value="">Choose a manufacturer</option>
                    {manufacturer.map(manufacturer => {
                        return (<option key={manufacturer.name} value={manufacturer.id}>{manufacturer.name}
                        </option>)
                    })}
                </select>
              </div>
            <button className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ModelForm;
