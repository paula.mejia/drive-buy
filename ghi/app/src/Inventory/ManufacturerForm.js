import React, {useState, useEffect} from 'react';

function ManufacturerForm() {
  const [name, setName] = useState('')

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
    }
  }

  useEffect(() => {
    fetchData();
}, [])

const handleNameChange = (e) => {
    setName(e.target.value);
}
const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {
      "name": name,
    }

    const manufacturerURL = 'http://localhost:8100/api/manufacturers/'
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(manufacturerURL, fetchConfig);

    if (response.ok) {
      const newManufacturer = await response.json();
      console.log(newManufacturer)

     setName('');
  }
}

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Manufacturer</h1>
          <form onSubmit={handleSubmit} id="add-manufacturer-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <button className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ManufacturerForm;
