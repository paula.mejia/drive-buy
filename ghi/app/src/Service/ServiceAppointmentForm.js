




import React, {useState, useEffect} from 'react';

function ServiceAppointmentForm() {
  const [technicians, setTechnicians] = useState([])
  const [formData, setFormData] = useState({
    automobile: '',
    customer_name: '',
    date: '',
    time: '',
    technician: '',
    reason: '',
  })

  const getData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const appointmentUrl = 'http://localhost:8080/api/service_rest/';
    console.log(formData)

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(appointmentUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        automobile: '',
        customer_name: '',
        date: '',
        time: '',
        technician: '',
        reason: '',
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      //Previous form data is spread (i.e. copied) into our new state object
      ...formData,

      //On top of the that data, we add the currently engaged input key and value
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new service appointment</h1>
          <form onSubmit={handleSubmit} id="create-serviceappointment-form">

            <div className="form-floating mb-3">
              <input onChange={handleFormChange}  value={formData.automobile} placeholder="automobile" required type="text" name="automobile" id="automobile" className="form-control" />
              <label htmlFor="automobile">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange}  value={formData.customer_name} placeholder="customer_name" required type="text" name="customer_name" id="customer_name" className="form-control" />
              <label htmlFor="customer_name">Customer name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.date} placeholder="date" required type="date" name="date" id="date" className="form-control" />
              <label htmlFor="date">Date</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.time} placeholder="time" required type="time" name="time" id="time" className="form-control" />
              <label htmlFor="time">Time</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.technician} required name="technician" id="technician" className="form-select">
                <option value="">Choose a technician</option>
                {technicians.map(technician => {
                  return (
                    <option key={technician.id} value={technician.id}>{technician.name}</option>
                  )
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" />
              <label htmlFor="name">Reason</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ServiceAppointmentForm;
