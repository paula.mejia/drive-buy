import React, {useState, useEffect} from 'react';

function TechnicianForm() {
  const [name, setName] = useState('')
  const [employee_number, setEmployeeNumber] = useState('')


  const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
    }
  }

  useEffect(() => {
    fetchData();
}, [])

const handleNameChange = (e) => {
    setName(e.target.value);
};
const handleEmployeeNumberChange = (e) => {
    setEmployeeNumber(e.target.value);
};

const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {
      "name": name,
      "employee_number": employee_number,
    }

    console.log(data)

    const techniciansURL = 'http://localhost:8080/api/technicians/'
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(techniciansURL, fetchConfig);

    if (response.ok) {
      const newTechnician = await response.json();
      console.log(newTechnician)

     setName('');
     setEmployeeNumber('');
  }
}

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a technician</h1>
          <form onSubmit={handleSubmit} id="add-technician-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEmployeeNumberChange} value={employee_number} placeholder="Employee number" required type="text" name="employee_number" id="employee_name" className="form-control" />
              <label htmlFor="employee_number">Employee Number</label>
            </div>
            <button className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default TechnicianForm;
