import React, {useState, useEffect} from 'react';

function SalesRecordForm() {
    const [automobiles, setAutomobiles] = useState([])
    const [salespeople, setSalesPeople] = useState([])
    const [customers, setCustomers] = useState([])
    const [formData, setFormData] = useState({
        automobile:'',
        sales_person:'',
        customer:'',
        price:'',

    })

    useEffect(() => {
        getData()
    }, []);

    async function getData() {
      const url = "http://localhost:8100/api/automobiles/";

      const response = await fetch(url);

      if (response.ok) {
          const data = await response.json();
          setAutomobiles(data.autos)
      }
    }

    const getData2 = async () => {
      const url = "http://localhost:8090/api/salespeople/";

      const response = await fetch(url);

      if (response.ok) {
          const data = await response.json();
          setSalesPeople(data)
      }
  }
    const getData3 = async () => {
      const url = "http://localhost:8090/api/customers/";

      const response = await fetch(url);

      if (response.ok) {
          const data = await response.json();
          setCustomers(data)
      }
  }


    useEffect(() => {
      getData();
    }, [])
    useEffect(() => {
      getData2();
    }, []);
    useEffect(() => {
      getData3();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();



        const salesURL = 'http://localhost:8090/api/sales/';
        console.log(formData)

        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        // const response = await fetch(automobileURL, salespersonURL, customerURL, fetchConfig);
        const response = await fetch(salesURL, fetchConfig);
        const newSale = await(response.json)
        console.log(newSale)

        if (response.ok) {
          const newSalesRecord = await response.json();
          console.log(newSalesRecord)

          setFormData({
            automobile: '',
            sales_person: '',
            customer: '',
            price: '',
          });
        }
      }

      const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
          //Previous form data is spread (i.e. copied) into our new state object
          ...formData,

          //On top of the that data, we add the currently engaged input key and value
          [inputName]: value
        });
      }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Record a new sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
            <div className="mb-3">
                <select value={formData.automobiles} onChange={handleFormChange} required id="automobile" name="automobile" className="form-select">
                    <option value="">Choose an automobile</option>
                    {automobiles.map(auto=> {
                        return (
                          <option key={auto.id} value={auto.id}>{auto.vin}</option>
                        )
                    })}
                </select>
              </div>
              <div className="mb-3">
                <select value={formData.sales_person} onChange={handleFormChange} required id="sales_person" name="sales_person" className="form-select">
                    <option value="">Choose a salesperson</option>
                    {salespeople.map(sales_person => {
                        return (<option key={sales_person.employee_number} value={sales_person.id}>{sales_person.name}
                        </option>)
                    })}
                </select>
              </div>
              <div className="mb-3">
                <select value={formData.customer} onChange={handleFormChange} required id="customer" name="customer" className="form-select">
                    <option value="">Choose a customer</option>
                    {customers.map(customer => {
                        return (<option key={customer.phone_number} value={customer.id}>{customer.name}
                        </option>)
                    })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.price} onChange={handleFormChange} placeholder="price" required type="price" name="price" id="price" className="form-control" />
                <label htmlFor="price">Price</label>
              </div>
              <button className="btn btn-primary">Record new sale</button>
            </form>
          </div>
        </div>
      </div>

    );
}

export default SalesRecordForm;
