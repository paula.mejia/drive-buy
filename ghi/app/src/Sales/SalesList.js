import { useState, useEffect} from 'react'

function SalesList() {
  const [sales, setSales] = useState([])

  const getData = async () => {
    const resp = await fetch('http://localhost:8090/api/sales/');

    if (resp.ok) {
      const data = await resp.json();
      setSales(data.sales)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  return (
    <>
    <div>
      <h1 className="display-5 fw-bold">List of Sales</h1>
    </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Sale person</th>
          <th>Employee number</th>
          <th>Customer</th>
          <th>VIN</th>
          <th>Sales price</th>
        </tr>
      </thead>
      <tbody>
        {sales.map(sale => {
          return (
            <tr>
              <td>{sale.sales_person.name}</td>
              <td>{ sale.sales_person.employee_number }</td>
              <td>{ sale.customer.name }</td>
              <td>{ sale.automobile.vin }</td>
              <td>${ sale.price }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}

export default SalesList;
