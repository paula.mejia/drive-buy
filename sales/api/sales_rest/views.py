from django.shortcuts import render
from django.http import JsonResponse
import json
from .models import AutomobileVO, SalesPerson, Customer, SalesRecord
from .encoders import AutomobileVOEncoder, SalesPersonEncoder, CustomerEncoder, SalesRecordEncoder
from django.views.decorators.http import require_http_methods

@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "POST":
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder = SalesPersonEncoder,
            safe = False,
        )
    else:
        salespeople = SalesPerson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder = SalesPersonEncoder,
            safe = False,
        )


@require_http_methods(["GET","POST"])
def api_list_customers(request):
    if request.method == "POST":
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder = CustomerEncoder,
            safe = False,
        )
    else:
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
            safe = False,
        )
@require_http_methods(["DELETE", "GET"])
def api_show_sales_person(request, id):
    if request.method == "GET":
        sales_person = SalesPerson.objects.get(id=id)
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonEncoder,
        )
    else:
        try:
            sales_person = SalesPerson.objects.get(id=id)
            sales_person.delete()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

@require_http_methods(["DELETE", "GET"])
def api_show_customer(request, id):
    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
        )
    else:
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@ require_http_methods(["GET","POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = SalesRecord.objects.all()
        return JsonResponse({"sales": sales}, encoder=SalesRecordEncoder)
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid VIN"},
                status = 404
            )
        try:
            sales_person = SalesPerson.objects.get(name=content["sales_person"])
            content["sales_person"] = sales_person
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "invalid name"},
                status = 404
            )
        try:
            customer = Customer.objects.get(name=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "invalid name"},
                status = 404
            )
        sale = SalesRecord.objects.create(**content)
        return JsonResponse(
            sale,
            encoder = SalesRecordEncoder,
            safe = False,
        )


@require_http_methods(["GET"])
def api_salesperson_history(request, employee_number):
    if request.method == "GET":
        sales_history = SalesRecord.objects.filter(sales_person_id = employee_number)
        return JsonResponse(
            {"sales_history": sales_history},
            encoder=SalesRecordEncoder,
        )


## this function is not utilized by front end (just used for testing in insomnia)
@require_http_methods(["GET"])
def api_automobileVOs(request):
    if request.method == "GET":
        autos = AutomobileVO.objects.all()
        return JsonResponse(
            autos,
            encoder=AutomobileVOEncoder,
            safe = False,
        )
