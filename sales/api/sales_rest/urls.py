"""sales_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from sales_rest.views import api_list_sales, api_list_salespeople, api_automobileVOs, api_list_customers, api_salesperson_history, api_show_sales_person, api_show_customer

urlpatterns = [
    path('admin/', admin.site.urls),
    path('sales/', api_list_sales, name="api_list_sales"),
    path('salespeople/', api_list_salespeople, name="api_list_salespeople"),
    path("salespeople/<int:id>/", api_show_sales_person, name="api_show_sales_person"),
    path('automobiles/', api_automobileVOs, name="api_automobileVOs",),
    path('customers/', api_list_customers, name="api_list_customers"),
    path("customers/<int:id>/", api_show_customer, name="api_show_customer"),
    path('salesperson_history/<int:employee_number>', api_salesperson_history, name="salesperson_history"),
]
